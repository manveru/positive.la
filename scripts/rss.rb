#!/usr/bin/env ruby
# frozen_string_literal: true

require 'rss'
require 'json'

input = JSON.parse(File.read(ARGV[0]))
items = [*input.delete('items')]

def set(parent, tree)
  return unless tree.is_a?(Hash)

  tree.each do |k, v|
    case v
    when Hash
      set(parent.send(k), v)
    else
      parent.send("#{k}=", v)
    end
  end
end

feed = RSS::Maker.make('rss2.0') do |maker|
  set(maker, input)
  maker.channel.updated = Time.now.to_s

  items.each do |post|
    maker.items.new_item do |item|
      meta = post.fetch('meta')
      item.link = "https://www.positive.la#{post.fetch('url')}"
      item.title = meta.fetch('title')
      item.updated = meta.fetch('date')
    end
  end
end

puts feed
