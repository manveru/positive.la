{
  description = "Flake for the Finesco website";

  inputs = {
    nixpkgs.follows = "euphenix/nixpkgs";
    nixpkgs-alejandra.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.follows = "euphenix/flake-utils";
    euphenix.url = "gitlab:manveru/euphenix/flake";
  };

  outputs = {
    self,
    euphenix,
    flake-utils,
    nixpkgs,
    ...
  } @ inputs:
    (flake-utils.lib.simpleFlake {
      inherit self nixpkgs;
      name = "site";
      preOverlays = [euphenix.overlay];
      overlay = import ./overlay.nix inputs;
      shell = ./shell.nix;
    })
    // {
      overlay = import ./overlay.nix;
    };
}
