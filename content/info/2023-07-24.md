---
date: 2023-07-24
title: 2023年 夏季休業日期間について
draft: false
---

<span class="colour" style="color:rgb(0, 0, 0)"><span class="colour" style="color:rgb(0, 0, 0)">平素は格別のお引き立てを賜り、厚く御礼申し上げます。</span>弊社は下記の期間を</span>夏季休業日<span class="colour" style="color:rgb(0, 0, 0)">とさせて頂きます。</span>

<br>
<span class="colour" style="color:rgb(0, 0, 0)">・</span>夏季<span class="colour" style="color:rgb(0, 0, 0)">休業期間　2</span>023年8月11日（金）～2023年8月18日（金）

<br>
<span class="colour" style="color:rgb(0, 0, 0)"><span class="colour" style="color:rgb(0, 0, 0)">インターネットでのお問い合わせ及びご注文は期間中も受け付けております。休業明け</span>2023年8月21日（月）より順次ご対応させて頂きます。</span>

<span class="colour" style="color:rgb(0, 0, 0)">誠に恐れ入りますが、どうぞよろしくお願いいたします。</span>
