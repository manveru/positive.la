inputs: final: prev: {
  site.site = prev.callPackage ./. {};
  alejandra = inputs.nixpkgs-alejandra.legacyPackages.${prev.system}.alejandra;
}
