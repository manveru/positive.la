{euphenix}: let
  inherit (euphenix.lib) take optionalString hasPrefix nameValuePair;
  inherit (euphenix) build loadPosts sortByRecent mkPostCSS cssTag;

  withLayout = body: [./templates/layout.html body];

  excludeDrafts = posts:
    euphenix.lib.filter (post: !(post.meta.draft or false)) posts;

  vars = rec {
    activeClass = route: prefix:
      optionalString (hasPrefix prefix route) "active";
    info = excludeDrafts (sortByRecent (loadPosts "/info/" ./content/info));
    latestBlogPosts = take 3 (sortByRecent info);
    liveJS = ''<script src="/js/live.js"></script>'';
    css = cssTag (mkPostCSS ./css);
  };

  route = template: title: id: {
    template = withLayout template;
    variables = vars // {inherit id title;};
  };

  mkRSS = feed:
    euphenix.mkDerivation {
      name = "rss";
      buildInputs = [euphenix.ruby euphenix.coreutils];
      buildCommand = ''
        mkdir -p $out
        ruby ${./scripts/rss.rb} ${
          __toFile "rss.json" (__toJSON feed)
        } > $out/feed
      '';
    };

  pageRoutes = {
    "/about/index.html" = route ./templates/about.html "会社概要" "about";
    "/contact/index.html" = route ./templates/contact.html "お問い合わせ" "contact";
    "/disclaimer/index.html" = route ./templates/disclaimer.html "免責事項" "disclaimer";
    "/info/index.html" = route ./templates/info.html "お知らせ" "blog";
    "/index.html" = route ./templates/home.html "Home" "home";
    "/privacy/index.html" = route ./templates/privacy.html "個人情報保護方針" "privacy";
    "/sent/en.html" =
      route ./templates/contact-sent-en.html "Thank you!" "sent";
    "/sent/jp.html" =
      route ./templates/contact-sent-jp.html "お問い合わせを受け付けました。" "sent";
  };

  infoRoutes = __listToAttrs (map (post:
    nameValuePair post.url {
      template = withLayout ./templates/post.html;
      variables =
        vars
        // post
        // {
          title = post.meta.title;
          id = "blog";
        };
    }) (vars.info));
in
  build {
    src = ./.;
    routes = pageRoutes // infoRoutes;
    favicon = ./static/images/favicon.svg;

    extraParts = let
      s = "ポジティブカンパニー合同会社";
      common = description: id: {
        channel = {
          inherit description;
          link = "https://www.atotori.com/${id}";
          title = "${s} - ${description}";
          language = "ja";
          generator = "EupheNix";
        };
        items = vars.${id};
      };
    in
      map (feed: mkRSS feed) [(common "お知らせ" "info")];
  }
